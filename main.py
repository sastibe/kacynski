#!/usr/bin/env python3

import argparse
import pandas as pd
from datetime import datetime, timedelta

list_of_holidays = ["01.01.2021", "01.05.2021","24.05.2021", "24.12.2021"]
list_of_holidays_dt = [datetime.strptime(date, '%d.%m.%Y') for date in list_of_holidays]

def calculate_hours_two_day_span(date, daily_hrs):

    date_dt = datetime.strptime(date, '%d.%m.%Y')
    weekday = datetime.weekday(date_dt)

    is_holiday_day1 = 0.0
    is_holiday_day2 = 0.0
    is_sunday_day1 = 0.0
    is_sunday_day2 = 0.0
    hrs_bd_day1 = 0.0
    hrs_bd_day2 = 0.0
    hrs_bdnacht_day1 = 0.0
    hrs_bdnacht_day2 = 0.0
    hrs_d_day1 = 0.0
    hrs_d_day2 = 0.0
    hrs_dnacht_day1 = 0.0
    hrs_dnacht_day2 = 0.0
    delta_hours = 0.0

    # Nachtarbeit von 21 bis 6 Uhr

    if weekday == 4: #Monday is 0 and Sunday is 6
        hrs_bd_day1 = 0.0
        hrs_bd_day2 = 4.25
        hrs_bdnacht_day1 = 0.0
        hrs_bdnacht_day2 = 3.5
        hrs_d_day1 = 5.5  
        hrs_d_day2 = 0.0
        hrs_dnacht_day1 = 2.5
        hrs_dnacht_day2 = 2.0
        delta_hours = hrs_d_day1 + hrs_d_day2 + hrs_dnacht_day1 + hrs_dnacht_day2 - daily_hrs
        
    elif weekday == 5:
        is_sunday_day2 = 1.0
        hrs_bd_day1 = 6.0
        hrs_bd_day2 = 4.5
        hrs_bdnacht_day1 = 3.0
        hrs_bdnacht_day2 = 6.0
        hrs_d_day1 = 5.17  
        hrs_d_day2 = 0.0
        hrs_dnacht_day1 = 0.0
        hrs_dnacht_day2 = 0.0
        delta_hours = hrs_d_day1 + hrs_d_day2 + hrs_dnacht_day1 + hrs_dnacht_day2
        
    elif weekday == 6:
        is_sunday_day1 = 1.0
        hrs_bd_day1 = 6.0
        hrs_bd_day2 = 3.75
        hrs_bdnacht_day1 = 3.0
        hrs_bdnacht_day2 = 6.0
        hrs_d_day1 = 5.17  
        hrs_d_day2 = 0.0
        hrs_dnacht_day1 = 0.0
        hrs_dnacht_day2 = 0.0
        delta_hours = hrs_d_day1 + hrs_d_day2 + hrs_dnacht_day1 + hrs_dnacht_day2 - daily_hrs
        
    else:
        hrs_bd_day1 = 0.0
        hrs_bd_day2 = 2.25
        hrs_bdnacht_day1 = 0.0
        hrs_bdnacht_day2 = 4.5
        hrs_d_day1 = 5.67  
        hrs_d_day2 = 0.0
        hrs_dnacht_day1 = 2.5
        hrs_dnacht_day2 = 1.0
        delta_hours = hrs_d_day1 + hrs_d_day2 + hrs_dnacht_day1 + hrs_dnacht_day2 - 2*daily_hrs

    if date_dt in list_of_holidays_dt:
        is_holiday_day1 = 1.0
        if date_dt + timedelta(days=1) in list_of_holidays_dt:
            is_holiday_day2 = 1.0
        else:
            is_holiday_day2 = 0.0
    else:
        is_holiday_day1 = 0.0
        if date_dt + timedelta(days=1) in list_of_holidays_dt:
            is_holiday_day2 = 1.0
        else:
            is_holiday_day2 = 0.0
        
    day1 = dict()
    day2 = dict()
    day1['hrs_bd'] = hrs_bd_day1
    day1['hrs_bdnacht'] = hrs_bdnacht_day1
    day1['hrs_d'] = hrs_d_day1
    day1['hrs_dnacht'] = hrs_dnacht_day1
    day1['is_holiday'] = is_holiday_day1
    day1['is_sunday']  = is_sunday_day1
    
    day2['hrs_bd'] = hrs_bd_day2
    day2['hrs_bdnacht'] = hrs_bdnacht_day2
    day2['hrs_d'] = hrs_d_day2
    day2['hrs_dnacht'] = hrs_dnacht_day2
    day2['is_holiday'] = is_holiday_day2
    day2['is_sunday']  = is_sunday_day2
        
    return pd.DataFrame([day1, day2], index = [date_dt, date_dt + timedelta(days=1)]), delta_hours


def summarize_hours_over_month(dates, daily_hrs):
    hours_per_day = pd.DataFrame([])
    delta_hours = 0.0
    
    for date in dates:
        df, dummy = calculate_hours_two_day_span(date, daily_hrs)
        hours_per_day = hours_per_day.append(df)
        delta_hours += dummy 

    hours_total = dict()
    hours_total['berdienst'] = hours_per_day['hrs_bd'].sum() + hours_per_day['hrs_bdnacht'].sum()
    hours_total['berdienst_nacht'] = hours_per_day['hrs_bdnacht'].sum()
    hours_total['berdienst_feiertag'] = hours_per_day.loc[ hours_per_day['is_holiday'] == 1.0 ,'hrs_bdnacht'].sum() + hours_per_day.loc[ hours_per_day['is_holiday'] == 1.0 ,'hrs_bd'].sum()
    hours_total['dienst_feiertag'] = hours_per_day.loc[ hours_per_day['is_holiday'] == 1.0 ,'hrs_dnacht'].sum() + hours_per_day.loc[ hours_per_day['is_holiday'] == 1.0 ,'hrs_d'].sum()
    hours_total['dienst_sonntag'] = hours_per_day.loc[ hours_per_day['is_sunday'] == 1.0 ,'hrs_dnacht'].sum() + hours_per_day.loc[ hours_per_day['is_sunday'] == 1.0 ,'hrs_d'].sum()
    hours_total['dienst_nacht'] = hours_per_day['hrs_dnacht'].sum()
    hours_total['delta_dienst'] = round(delta_hours, 2)
    
    return hours_total

def monthly_account(dates, daily_hrs = 8):
    dict_hours = summarize_hours_over_month(dates, daily_hrs)
    df_res =  pd.DataFrame({
        'factor'      : [1, 1, 1, 1, 1.1, 1.1],
        'rate'        : [10.31, 14.43, 6.19, 5.4, 35.97, 8.99]
    }, index = ['JNN ZZ Sonntag',
                'JNN FeiertgZuschlag',
                'JNN Nachtarbeit-ZuSchl',
                'JNN ZZ Nachtarb 15%',
                'JLL BereitDienst',
                'JNN FZZ Ber.dienst'])

    df_res['hrs'] = 0.0
    df_res.at['JNN ZZ Sonntag', 'hrs']          = dict_hours['dienst_sonntag']
    df_res.at['JNN FeiertgZuschlag', 'hrs']     = dict_hours['dienst_feiertag']
    df_res.at['JNN Nachtarbeit-ZuSchl' , 'hrs'] = dict_hours['dienst_nacht']
    df_res.at['JNN ZZ Nachtarb 15%', 'hrs']     = dict_hours['berdienst_nacht']
    df_res.at['JLL BereitDienst', 'hrs']        = dict_hours['berdienst']
    df_res.at['JNN FZZ Ber.dienst' , 'hrs']     = dict_hours['berdienst_feiertag']

    df_res['total'] = round( df_res['hrs'] * df_res['rate'] * df_res['factor'], 2)

    
    return df_res



CLI=argparse.ArgumentParser()

CLI.add_argument(
    "dienste", 
    nargs="*",
    type=str,
    default=["01.01.2021"], 
)

CLI.add_argument(
    "--daily_hours", #number of expected daily hours 
    nargs='?',
    type=int, 
    default=8,
)



args = CLI.parse_args()
print(monthly_account(args.dienste, args.daily_hours))
